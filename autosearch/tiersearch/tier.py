"""
qb_assessment
"""
import os
import sys

import django

sys.path.append("..")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "autosearch.settings")
if __name__ == "__main__":
    django.setup()

import urllib.request


class TierMethodSearch:
    """search"""

    def __init__(self, assessement_details=None):
        """INIT"""
        if assessement_details:
            self.unique_test_id = assessement_details["unique_test_id"]
            self.assessment_details = assessement_details
            self.errl = []
            self.successl = []
        else:
            print('im here')
            self.errl = []
            self.successl = []

    def get_random_words(self):
        word_url = "http://svnweb.freebsd.org/csrg/share/dict/words?view=co&content-type=text/plain"
        response = urllib.request.urlopen(word_url)
        long_txt = response.read().decode()
        words = long_txt.splitlines()
        print(words)
        return words


class TrieNode():
    def __init__(self):
        # Initialising one node for trie
        self.children = {}
        self.last = False


class Trie():

    def __init__(self):
        # Initialising the trie structure.
        self.root = TrieNode()
        self.word_list = []

    def formTrie(self, keys):

        # Forms a trie structure with the given set of strings
        # if it does not exists already else it merges the key
        # into it by extending the structure as required
        for key in keys:
            self.insert(key)  # inserting one key to the trie.

    def insert(self, key):

        # Inserts a key into trie if it does not exist already.
        # And if the key is a prefix of the trie node, just
        # marks it as leaf node.
        node = self.root

        for a in list(key):
            if not node.children.get(a):
                node.children[a] = TrieNode()

            node = node.children[a]

        node.last = True

    def search(self, key):

        # Searches the given key in trie for a full match
        # and returns True on success else returns False.
        node = self.root
        found = True

        for a in list(key):
            if not node.children.get(a):
                found = False
                break

            node = node.children[a]

        return node and node.last and found

    def suggestionsRec(self, node, word):
        # Method to recursively traverse the trie
        # and return a whole word.
        if node.last:
            self.word_list.append(word)

        for a, n in node.children.items():
            self.suggestionsRec(n, word + a)

    def printAutoSuggestions(self, key):

        # Returns all the words in the trie whose common
        # prefix is the given key thus listing out all
        # the suggestions for autocomplete.
        node = self.root
        not_found = False
        temp_word = ''

        for a in list(key):
            if not node.children.get(a):
                not_found = True
                break

            temp_word += a
            node = node.children[a]

        if not_found:
            return 0
        elif node.last and not node.children:
            return -1

        self.suggestionsRec(node, temp_word)
        final_op = []
        for s in self.word_list:
            print(s)
            final_op.append(s)
        return final_op


if __name__ == "__main__":
    print("hello")
    obj = TierMethodSearch()
    data = obj.get_random_words()
    print(data[:100])

    # keys = ["hello", "dog", "hell", "cat", "a",
    #     "hel", "help", "helps", "helping"] # keys to form the trie structure.
    key = "ab"  # key for autocomplete suggestions.
    status = ["Not found", "Found"]

    # creating trie object
    t = Trie()

    # creating the trie structure with the
    # given set of strings.
    t.formTrie(data[:100])

    # autocompleting the given key using
    # our trie structure.
    comp = t.printAutoSuggestions(key)

    if comp == -1:
        print("No other strings found with this prefix\n")
    elif comp == 0:
        print("No string found with this prefix\n")
