# djangotemplates/example/views.py
from django.shortcuts import render
from django.views.generic import TemplateView # Import TemplateView
from rest_framework.views import status, APIView
from rest_framework.response import Response
from .tier import Trie, TierMethodSearch
from .words import words

# Add the two views we have been talking about  all this time :)

def Index(request):
    return render(request, 'index.html')


class AutoSuggest(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        # obj = TierMethodSearch()
        data = words
        print(data)

        key = request.query_params.get('key')
        # status = ["Not found", "Found"]
        print(key, 'key')
        print(type(key), 'type')


        t = Trie()
        t.formTrie(data)

        # autocompleting the given key using
        # our trie structure.
        comp = t.printAutoSuggestions(key)


        data = "success"
        return Response(comp)
