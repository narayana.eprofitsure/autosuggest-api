from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.Index, name='home'), # Notice the URL has been named
    url(r'autosuggest/$', views.AutoSuggest.as_view()),
]
